import React from 'react'
import './Footer.scss'
import FbIcon from './Icons/fb icon.png'
import TwIcon from './Icons/tw icon.png'
import IgIcon from './Icons/ig icon.png'

const Footer: React.FC = () => {
    return (
        <div className='Footer'>

            <div className='Footer-top'>

                <div>
                    <h3>
                        PROFILE
                    </h3>
                    <ul>
                        <li>FAQ’s</li>
                        <li>Pricing plans</li>
                        <li>Order tracking</li>
                        <li>Returns</li>
                    </ul>
                </div>

                <div>
                    <h3>
                        RECENT POSTS
                    </h3>
                    <ul>
                        <li>Touch of uniqueness</li>
                        <li>Offices you won’t forget</li>
                        <li>Cicilan</li>
                    </ul>
                </div>

                <div>
                    <h3>
                        CUSTOMER
                    </h3>
                    <ul>
                        <li>Help & contact us</li>
                        <li>Return</li>
                        <li>Online stores</li>
                        <li>Terms & cordition</li>
                    </ul>
                </div>

                <div>
                    <h3>
                        CONTACT
                    </h3>
                    <ul className='Icons'>
                        <li><img src={IgIcon} alt="instagram" /></li>
                        <li><img src={TwIcon} alt="twitter" /></li>
                        <li><img src={FbIcon} alt="facebook" /></li>

                    </ul>
                </div>

            </div>

            <hr />

            <div className='Footer-bottom'>
                <p>
                    © 2014 Nizami cinema. All Right Reserved
                </p>
            </div>
        </div>
    )
}

export default Footer