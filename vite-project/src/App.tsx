import React from 'react'
import Footer from './Footer/Footer'

const App: React.FC = () => {
  return (
    <>
      <Footer />
    </>
  )
}

export default App